package com.booston.filemanager3d.activity;

import com.booston.filemanager3d.R;
import com.booston.filemanager3d.adapter.FileVpAdapter;
import com.booston.filemanager3d.utils.FileUtils;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.SimpleOnPageChangeListener;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

/**
* @author 陈红华 E-mail:cenghonho@126.com
* @version 创建时间：2016年4月11日 上午10:29:25
* 类说明 :文件管理的主界面
*/
public class FileManagerActivity extends Activity implements OnClickListener{
	
	/** 文件管理主界面view容器 */
	private LinearLayout baseView;
	/** 做面板布局 */
	private LinearLayout leftPart;
	/** 右面板布局 */
	private LinearLayout rightPart;
	/** 已用內存显示文本 */
	private TextView lt_useMemoryTv;
	private TextView rt_useMemoryTv;
	/** 总的內存显示文本 */
	private TextView lt_avaMemoryTv;
	private TextView rt_avaMemoryTv;
	/** 表示内存使用情况的进度条 */
	private ProgressBar rt_memoryProBar;
	private ProgressBar lt_memoryProBar;
	/** 展示功能列表的Gallery */
	private ViewPager mVpLeft;
	private ViewPager mVpRight;
	private DisplayMetrics outMetrics;
	/** ViewPager适配器 */
	private FileVpAdapter adapter;
	private Button btNextLeft;
	/** ViewPager当前页面*/
	public int number;
	private Button btBackLeft;
	private Button btOkLeft;
	private Button btNextRight;
	private Button btBackRight;
	private Button btOkRight;
	/** 列表的当前选中页 */
	private int currentItem;
	private SimpleOnPageChangeListener onPageChangeListener;
    /** 记录当前选中item的图标 */
	public int listnumber = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		initView();
		initData();
		setListener();
	}

	/** 初始化界面视图 */
	private void initView() {
		//去掉标题栏
        requestWindowFeature(Window.FEATURE_NO_TITLE);
		//设置为全屏
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		outMetrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(outMetrics);
		baseView = (LinearLayout) View.inflate(this, R.layout.activity_base, null);
		leftPart = ((LinearLayout) View.inflate(this, R.layout.filemanager_main,
				null));
		rightPart = ((LinearLayout) View.inflate(this, R.layout.filemanager_main,
				null));
		lt_useMemoryTv = (TextView) leftPart.findViewById(R.id.tv_memory_use);
		lt_avaMemoryTv = (TextView) leftPart.findViewById(R.id.tv_memory_total);
		lt_memoryProBar = (ProgressBar) leftPart.findViewById(R.id.probar_filemanager_memory);
		
		rt_useMemoryTv = (TextView) rightPart.findViewById(R.id.tv_memory_use);
		rt_avaMemoryTv = (TextView) rightPart.findViewById(R.id.tv_memory_total);
		rt_memoryProBar = (ProgressBar) rightPart.findViewById(R.id.probar_filemanager_memory);
		mVpLeft = (ViewPager)leftPart.findViewById(R.id.vp_filemanager);
		mVpRight = (ViewPager)rightPart.findViewById(R.id.vp_filemanager);
		
		btNextLeft = (Button) leftPart.findViewById(R.id.bt_filemager_next);
		btBackLeft = (Button) leftPart.findViewById(R.id.bt_filemager_back);
		btOkLeft = (Button) leftPart.findViewById(R.id.bt_filemager_ok);
		
		btNextRight = (Button) rightPart.findViewById(R.id.bt_filemager_next);
		btBackRight = (Button) rightPart.findViewById(R.id.bt_filemager_back);
		btOkRight = (Button) rightPart.findViewById(R.id.bt_filemager_ok);
		
		//设置子view的属性
		LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(-1, -1);
		params.weight = 1.0F;
		baseView.addView(leftPart,params);
		baseView.addView(rightPart,params);
		setContentView(baseView);
	}
	
	/** 设置界面的数据 */
    private void initData() {
    	adapter = new FileVpAdapter(this);
    	mVpLeft.setAdapter(adapter);// 绑定适配器
		mVpRight.setAdapter(adapter);// 绑定适配器
    	String useMemory = FileUtils.getUseMemory(this);
    	String avameoy = FileUtils.getAvailableInternalMemorySize(this);
    	int titalMeoy = FileUtils.getTotalInternalMemorySize();
    	int useMemoy = FileUtils.getUseMemory();
    	lt_useMemoryTv.setText("已用:" + useMemory);
    	lt_avaMemoryTv.setText("可用:" + avameoy);
    	lt_memoryProBar.setMax(titalMeoy);
    	lt_memoryProBar.setProgress(useMemoy);
    	rt_memoryProBar.setMax(titalMeoy);
    	rt_memoryProBar.setProgress(useMemoy);
    	rt_useMemoryTv.setText("已用:" + useMemory);
    	rt_avaMemoryTv.setText("可用:" + avameoy);
	}
    
    /** 设置事件监听*/
    private void setListener() {
    	//做面板监听
    	btBackLeft.setOnClickListener(this);
    	btNextLeft.setOnClickListener(this);
    	btOkLeft.setOnClickListener(this);
    	//右面板监听
    	btBackRight.setOnClickListener(this);
    	btNextRight.setOnClickListener(this);
    	btOkRight.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.bt_filemager_back://上一步
			btLast();
			break;
        case R.id.bt_filemager_ok://ok键
        	btOk();
			break;
        case R.id.bt_filemager_next://下一步
        	btNext();
        	break;
	     }
    }

	/** 下一步*/
	private void btNext() {
		switch (listnumber) {
		case 0:
		case 1:
		case 2:
		case 3:
			listnumber++;
			upIcon();
			if(listnumber == 3 || listnumber ==4){
				mVpLeft.setCurrentItem(1);
				mVpRight.setCurrentItem(1);
			}
			break;
		case 4:
			listnumber = 0;
			upIcon();
			mVpLeft.setCurrentItem(0);
			mVpRight.setCurrentItem(0);
			break;
		}
	}

	/** 确定 */
	private void btOk() {
		Intent intent = new Intent();
		switch (listnumber) {
		case 0://全部
			intent.setClass(FileManagerActivity.this, FileActivity.class);
			break;
		case 1://视频
			intent.setClass(FileManagerActivity.this,VideoActivity.class);
			break;
		case 2://图片
			intent.setClass(FileManagerActivity.this, PictureActivity.class);
			break;
		case 3://音乐
			intent.setClass(FileManagerActivity.this, ApkInfoActivity.class);
			break;
		case 4://安装
			intent.setClass(FileManagerActivity.this, AudioActivity.class);
			break;
		}
		startActivity(intent);
	}

	/** 上一步 */
	private void btLast() {
		switch (listnumber) {
		case 1:
		case 2:
		case 3:
			listnumber--;
			upIcon();
			if(listnumber == 2){
				mVpLeft.setCurrentItem(0);
				mVpRight.setCurrentItem(0);
			}
			break;
		case 0:
		case 4:
			if(listnumber == 0) listnumber = 4;
			else listnumber--;
			upIcon();
			mVpLeft.setCurrentItem(1);
			mVpRight.setCurrentItem(1);
			break;
		}
	}
	/**
	 * 更新背景图标的方法（更新适配器）
	 */
	public void upIcon() {
		adapter.setCurSelectedIndex(listnumber);
		mVpLeft.setAdapter(adapter);// 绑定适配器
		mVpRight.setAdapter(adapter);// 绑定适配器
		adapter.notifyDataSetChanged();
	}
	
}
