package com.booston.filemanager3d.activity;

import java.io.File;
import java.util.ArrayList;

import com.booston.filemanager3d.bean.MediaInfo;
import com.booston.filemanager3d.utils.FileUtils;

import android.content.Intent;
import android.net.Uri;
import android.os.Environment;

/**
* @author 陈红华 E-mail:cenghonho@126.com
* @version 创建时间：2016年4月14日 上午11:25:07
* 类说明:图片界面
*/
public class PictureActivity extends MediaActivity{
	
	private ArrayList<MediaInfo> pictureFiles = new ArrayList<MediaInfo>();
	
	@Override
	protected void initData() {
		super.initData();
		task = new MediaAsyncTask() {
			@Override
			public ArrayList<MediaInfo> getFileList() {
				pictureFiles.clear();
				inflateGridView(new File(Environment.getExternalStorageDirectory().getPath()));
				return pictureFiles;
			}
		}.setConfig(PictureActivity.this, mGridViewLeft, mGridViewRight);
		task.execute();
	}
	
	@Override
	protected void btOk() {
		info = task.getMediaAdapter().getItem(currentItem);
		Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setDataAndType(Uri.parse("file://" + info.path), "image/*");
        startActivity(intent);
	}
	
	public void inflateGridView(File file) {
		// 如果是文件那么我获取文件名字再判断是否是图片文件
		if (file.isFile() && FileUtils.isPicture(file.getName())) {
			MediaInfo myFile = new MediaInfo();
			myFile.title = file.getName();
			myFile.path = file.getAbsolutePath();
			myFile.bitmap = FileUtils.getImageThumbnail(file.getAbsolutePath(), 100, 100);
			pictureFiles.add(myFile);
		} else if (file.isDirectory()) {
			if (file.getName().equals("Android")) {
				return;
			}
			File[] listFiles = file.listFiles();
			if(listFiles != null)
			// 循环文件数组里面的文件，再递归调用自己
			for (File file1 : listFiles) {
				inflateGridView(file1);
			}
		}
	}
}
