package com.booston.filemanager3d.activity;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import com.booston.filemanager3d.R;
import com.booston.filemanager3d.adapter.ApkInfoAdapter;
import com.booston.filemanager3d.bean.ApkFileInfo;
import com.booston.filemanager3d.utils.FileUtils;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.view.View;
import android.widget.TextView;

/**
* @author 陈红华 E-mail:cenghonho@126.com
* @version 创建时间：2016年4月12日 上午10:09:36
* 类说明:安装包界面
*/
public class ApkInfoActivity extends BaseActivity{
	
	private List<ApkFileInfo> apkFiles = new ArrayList<ApkFileInfo>();
	/** 取消安装按钮*/
	private TextView tvCancel;
	/** 确定安装按钮*/
	private TextView tvOk;
	/** apk安装对话框*/
	private AlertDialog installDialog;
	/** 安装取消标识位 */
	private boolean isCancel;
	private ApkFileInfo info;
	private ApkInfoAdapter adapter;
	@Override
	protected void initData() {
		super.initData();
		new MyTask().execute();
	}
	
	class MyTask extends AsyncTask<Void, Void, List<ApkFileInfo>>{

		@Override
		protected List<ApkFileInfo> doInBackground(Void... params) {
			apkFiles.clear();
			inflateGridView(new File(Environment.getExternalStorageDirectory().getPath()));
			return apkFiles;
		}
		
		@Override
		protected void onPostExecute(List<ApkFileInfo> result) {
			super.onPostExecute(result);
			adapter = new ApkInfoAdapter(ApkInfoActivity.this,result);
			mGridViewLeft.setAdapter(adapter);
			mGridViewRight.setAdapter(adapter);
		}
	}
	
	/** 显示apk安装对话框 */
	private void showInstallDialog(){
		Builder builder = new AlertDialog.Builder(this);
		View view = View.inflate(this, R.layout.apk_anzhuang_info_dialog_layout, null);
		tvCancel = (TextView) view.findViewById(R.id.bt_apk_dialog_cancal);
		tvOk = (TextView) view.findViewById(R.id.bt_apk_dialog_ok);
		builder.setView(view);
		installDialog = builder.create();
		installDialog.show();
	}
	
	@Override
	protected void btBack() {
		finish();//返回上一个界面
	}
	@Override
	protected void btLast() {
		currentItem--;
		if(currentItem < 0){
			if(adapter != null)
			currentItem = adapter.getCount() - 1;
		}
		if(adapter != null)
		adapter.setSelectPosition(currentItem);
	}
	
	@Override
	protected void btNext() {
		currentItem++;
		if(adapter != null)
		if(currentItem >= adapter.getCount()){
			currentItem = 0;
		}
		if(adapter != null)
		adapter.setSelectPosition(currentItem);
	}
	
	@Override
	protected void btOk() {
		//TODO 安装apk
		info = apkFiles.get(currentItem); 
		Intent intent = new Intent(Intent.ACTION_VIEW); 
		intent.addCategory(Intent.CATEGORY_DEFAULT);
		 intent.setDataAndType(Uri.fromFile(info.file),  
	                "application/vnd.android.package-archive");  
        startActivity(intent);  
	}

	public void inflateGridView(File file) {
		// 如果是文件那么我获取文件名字再判断是否是图片文件
		if (file.isFile() && FileUtils.isApk(file.getName())) {
			ApkFileInfo myFile = new ApkFileInfo();
			myFile.file = file;
			myFile.apkName = file.getName();
        	myFile.path = file.getPath();// apk文件的绝对路劲
            PackageManager pm = this.getPackageManager();
            PackageInfo packageInfo = pm.getPackageArchiveInfo(myFile.path, PackageManager.GET_ACTIVITIES);
            ApplicationInfo appInfo = packageInfo.applicationInfo;
             //获取apk的图标 
            myFile.icon = appInfo.loadIcon(pm);
            //得到包名 
            myFile.name = packageInfo.packageName;
            apkFiles.add(myFile);
		} else if (file.isDirectory()) {
			if (file.getName().equals("Android")) {
				return;
			}
			File[] listFiles = file.listFiles();
			if(listFiles != null)
			// 循环文件数组里面的文件，再递归调用自己
			for (File file1 : listFiles) {
				inflateGridView(file1);
			}
		}
	}
}
