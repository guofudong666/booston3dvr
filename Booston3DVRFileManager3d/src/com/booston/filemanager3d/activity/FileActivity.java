package com.booston.filemanager3d.activity;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import com.booston.filemanager3d.R;
import com.booston.filemanager3d.adapter.FileListAdapter;

import android.os.Environment;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.PopupWindow;
import android.widget.Toast;

/**
* @author 陈红华 E-mail:cenghonho@126.com
* @version 创建时间：2016年4月11日 下午8:00:06
* 类说明 ：所有文件界面
*/
public class FileActivity extends BaseActivity {
    private ArrayList<Map<String,Object>> aList;
	private FileListAdapter adapter;
    private static final String ROOT_PATH = "/";  
    //存储文件名称  
    private ArrayList<String> names = new ArrayList<String>();  
    //存储文件路径  
    private ArrayList<String> paths = new ArrayList<String>(); 
    
    File currentParent;
	// 记录当前路径下的所有文件的文件数组
	File[] currentFiles;
	List<Map<String, Object>> GridItems = new ArrayList<Map<String, Object>>();;

	@Override
    protected void initData() {
    	super.initData();
    	getFile();
    	adapter = new FileListAdapter(this,GridItems);
    	mGridViewLeft.setSelector(R.drawable.list_item_selected);
    	mGridViewLeft.setAdapter(adapter); 
    	mGridViewRight.setSelector(R.drawable.list_item_selected);
    	mGridViewRight.setAdapter(adapter); 
        
    }
  
	@Override
	protected void btLast() {
		currentItem--;
		if(currentItem < 0){
			if(adapter != null){
				currentItem = adapter.getCount() - 1;
			}
		}
		if(adapter != null)
		adapter.setSelectPosition(currentItem);
	}
	@Override
	protected void btNext() {
		currentItem++;
		if(adapter != null)
    	if(currentItem >= adapter.getCount()){
    		currentItem = 0;
    	}
    	if(adapter != null)
    	adapter.setSelectPosition(currentItem);
	}
	
	@Override
	protected void btBack() {
		try {
			// 如果当前目录不是最顶层的目录
			if (!currentParent.getCanonicalFile().equals(Environment.getExternalStorageDirectory().getPath())) {
				// 我就获取上一级目录
				currentParent = currentParent.getParentFile();
				// 列出当前目录下所有文件
				if(currentParent != null)
				currentFiles = currentParent.listFiles();
				else finish();
				// 我再更新列表
				inflateGridView(currentFiles);
				adapter.notifyDataSetChanged();
			}else{
				finish();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	@Override
	protected void btOk() {
		// 用户单击了文件，直接返回，不做任何处理
		if (currentFiles[currentItem].isFile()) {
			showFileOperateDialog();
			return;
		}
		// 获取用户点击的文件夹下的所有文件
		File[] tmp = currentFiles[currentItem].listFiles();
		// 如果文件为空那麽提示
		if (tmp == null || tmp.length == 0) {
			Toast.makeText(FileActivity.this, "空文件夹", Toast.LENGTH_SHORT).show();
		} else {
			// 否則獲取单擊的列表项对应的文件夹，设為當前的父文件夾
			currentParent = currentFiles[currentItem];
			// 保存當前的父文件夾內的全部文件和文件夾
			currentFiles = tmp;
			inflateGridView(currentFiles);
			//进入子目录，置为初始状态
			currentItem = 0;
			adapter.setSelectPosition(currentItem);
		}
	}
	

    /**显示文件操作对话框  */
	private void showFileOperateDialog(){
		View lt_view = View.inflate(this, R.layout.file_operate_dialog_layout, null);
		 PopupWindow lt_popup = new PopupWindow(lt_view,
	                LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, false);
		 // 如果不设置PopupWindow的背景，无论是点击外部区域还是Back键都无法dismiss弹框
		/*lt_popup.setBackgroundDrawable(getResources().getDrawable(
                R.drawable.background));*/
        // 设置好参数之后再show
		lt_popup.showAtLocation(lt_btOk, Gravity.CENTER, -300,20);
		
		View rt_view = View.inflate(this, R.layout.file_operate_dialog_layout, null);
		 PopupWindow rt_popup = new PopupWindow(rt_view,
	                LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, false);
		 // 如果不设置PopupWindow的背景，无论是点击外部区域还是Back键都无法dismiss弹框
		/*lt_popup.setBackgroundDrawable(getResources().getDrawable(
               R.drawable.background));*/
       // 设置好参数之后再show
		rt_popup.showAtLocation(rt_btOk, Gravity.CENTER, 300,20);
	}
	
	/**
	 * 获取系统文件的
	 */
	public void getFile() {
		// 获取系统的根目录
		File root = new File(ROOT_PATH);
		// 记录当前父目录
		currentParent = root;
		// 返回此文件所代表的目录中包含的文件的数组。
		currentFiles = root.listFiles();
		// 使用当前目录下的全部文件、文件夹来填充GridtView
		inflateGridView(currentFiles);
	}
 
    /**
     * 将文件列表添加到GridView的数据集合中
     * @param files
     */
	private void inflateGridView(File[] files) {
		// 创建一个List集合，List集合的元素是Map
		GridItems.clear();
		for (int i = 0; i < files.length; i++) {
			Map<String, Object> GridItem = new TreeMap<String, Object>();
			// 如果当前File是文件夹，使用folder图标；否则使用file图标
			if (files[i].isDirectory()) {
				GridItem.put("icon", R.drawable.folder);
			} else {
				GridItem.put("icon", R.drawable.file);
			} // 获取名字
			GridItem.put("fileName", files[i].getName());
			// 添加List项
			GridItems.add(GridItem);
		}
	}
	
}
