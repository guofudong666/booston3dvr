package com.booston.filemanager3d.activity;

import com.booston.filemanager3d.R;
import com.booston.filemanager3d.bean.MediaInfo;

/**
* @author 陈红华 E-mail:cenghonho@126.com
* @version 创建时间：2016年4月14日 下午2:07:58
* 类说明:多媒体界面的父类
*/
public class MediaActivity extends BaseActivity{
	
	/** 当前选中项的视频信息 */
	protected MediaInfo info;
	
	public MediaAsyncTask task;
	
	@Override
	protected void initData() {
		super.initData();
		mGridViewLeft.setSelector(R.drawable.list_item_selected);
		mGridViewRight.setSelector(R.drawable.list_item_selected);
	}
	
	@Override
	protected void btBack() {
		finish();
	}
	
	@Override
	protected void btLast() {
		currentItem--;
		if(currentItem < 0){
			if(task.getMediaAdapter() != null)
			currentItem = task.getMediaAdapter().getCount() - 1;
		}
		task.getMediaAdapter().setSelectPosition(currentItem);
	}
	
	@Override
	protected void btNext() {
		currentItem++;
		if(task.getMediaAdapter() != null)
		if(currentItem >= task.getMediaAdapter().getCount()){
			currentItem = 0;
		}
		task.getMediaAdapter().setSelectPosition(currentItem);
	}
	
	@Override
	protected void btOk() {
		super.btOk();
	}

}
