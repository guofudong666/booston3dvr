package com.booston.filemanager3d.activity;

import java.util.ArrayList;

import com.booston.filemanager3d.adapter.MediaAdapter;
import com.booston.filemanager3d.bean.MediaInfo;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.GridView;

/**
* @author 陈红华 E-mail:cenghonho@126.com
* @version 创建时间：2016年4月14日 下午1:32:38
* 类说明:多媒体异步查询任务
*/
public abstract class MediaAsyncTask extends AsyncTask<Void,Void,ArrayList<MediaInfo>> {
	
	private MediaAdapter adapter;
	private Context context;
	private GridView mGridViewLeft;
	private GridView mGridViewRight;

	@Override
	protected ArrayList<MediaInfo> doInBackground(Void... params) {
		return getFileList();
	}
	
	@Override
	protected void onPostExecute(ArrayList<MediaInfo> result) {
		super.onPostExecute(result);
		if (result != null) {
			adapter = new MediaAdapter(context, result);
			mGridViewLeft.setAdapter(adapter);
			mGridViewRight.setAdapter(adapter);
			adapter.notifyDataSetChanged();
		}
	}

	public abstract ArrayList<MediaInfo> getFileList();
	public MediaAsyncTask setConfig(Context context,GridView g1,GridView g2){
		this.context = context;
		mGridViewRight = g1;
		mGridViewLeft = g2;
		return this;
	}
	
	/**
	 * 返回多媒体文件列表输配器
	 * @return
	 */
	public MediaAdapter getMediaAdapter(){
		return adapter;
	}

}
