package com.booston.filemanager3d.activity;

import java.util.ArrayList;

import com.booston.filemanager3d.bean.MediaInfo;
import com.booston.filemanager3d.utils.MediaUtils;

import android.content.Intent;
import android.net.Uri;

/**
 * @author 陈红华 E-mail:cenghonho@126.com
 * @version 创建时间：2016年4月14日 上午11:25:07
 * 视频管理界面
 */
public class VideoActivity extends MediaActivity {
	
    protected void initData() {
    	super.initData();
		task = new MediaAsyncTask() {
			@Override
			public ArrayList<MediaInfo> getFileList() {
				return MediaUtils.getVideoList(VideoActivity.this);
			}
		}.setConfig(VideoActivity.this, mGridViewLeft, mGridViewRight);
		task.execute();
	}
	
	@Override
	protected void btOk() {
		if(task.getMediaAdapter() != null)
		info = task.getMediaAdapter().getItem(currentItem);
		Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setDataAndType(Uri.parse("file://" + info.path), "video/MP4");
        startActivity(intent);
	}
	
}
