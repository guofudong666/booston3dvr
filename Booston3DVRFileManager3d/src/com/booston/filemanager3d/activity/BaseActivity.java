package com.booston.filemanager3d.activity;

import com.booston.filemanager3d.R;
import com.booston.filemanager3d.utils.FileUtils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

/**
* @author 陈红华 E-mail:cenghonho@126.com
* @version 创建时间：2016年4月11日 下午7:15:53
* 类说明:activity的父类
*/
@SuppressLint("NewApi")
public class BaseActivity extends Activity implements OnClickListener{
	
	/** 外部存储使用容量 */
	private TextView lt_UseMemoryUTv;
	
	/** 外部存储可用容量 */
	private TextView lt_AvaMemoryUTv;
	
	/** 内部存储使用容量 */
	private TextView lt_UseMemoryTv;
	
	/** 内部存储可用容量 */
	private TextView lt_AvaMemoryTv;
	
	/** 外部存储使用进度条 */
	private ProgressBar lt_MemoryUPb;
	
	/** 内部存储使用进度条量 */
	private ProgressBar lt_MemoryPb;
	
	/** 外部存储使用容量 */
	private TextView rt_UseMemoryUTv;
	
	/** 外部存储可用容量 */
	private TextView rt_AvaMemoryUTv;
	
	/** 内部存储使用容量 */
	private TextView rt_UseMemoryTv;
	
	/** 内部存储可用容量 */
	private TextView rt_AvaMemoryTv;
	
	/** 外部存储使用进度条 */
	private ProgressBar rt_MemoryUPb;
	
	/** 内部存储使用进度条量 */
	private ProgressBar rt_MemoryPb;

	/** 展示信息的GrideView */
	protected GridView mGridViewLeft;
	protected GridView mGridViewRight;

	private LinearLayout baseView;

	/** 左面板 */
	protected LinearLayout leftPart;

	/** 右面板 */
	protected LinearLayout rightPart;

	/** 左边返回按键 */
	private Button lt_btBack;

	/** 左边确定按键 */
	protected Button lt_btOk;

	/** 左边下一个按键 */
	protected Button lt_btNext;

	/** 左边上一个按键 */
	private Button lt_btLast;

	/** 右边返回按键 */
	private Button rt_btBack;

	/** 右边确定按键 */
	protected Button rt_btOk;

	/** 右边下一个按键 */
	private Button rt_btNext;

	/** 右边上一个按键 */
	private Button rt_btLast;
	
	/** 当前的选中的项 */
	protected int currentItem;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		initView();
		initData();
		setListener();
	}

	/** 初始化界面视图 */
	private void initView() {
		leftPart = ((LinearLayout) View.inflate(this, R.layout.activity_base_layout,
				null));
		rightPart = ((LinearLayout) View.inflate(this, R.layout.activity_base_layout,
				null));
		//设置子view的属性
		LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(-1, -1);
		params.weight = 1.0F;
		baseView = (LinearLayout) View.inflate(this, R.layout.activity_base, null);
		lt_UseMemoryUTv = (TextView) leftPart.findViewById(R.id.tv_memory_use_upan);
		lt_AvaMemoryUTv = (TextView) leftPart.findViewById(R.id.tv_memory_ava_upan);
		lt_UseMemoryTv = (TextView) leftPart.findViewById(R.id.tv_memory_use);
		lt_AvaMemoryTv = (TextView) leftPart.findViewById(R.id.tv_memory_ava);
		lt_MemoryUPb = (ProgressBar) leftPart.findViewById(R.id.probar_memory_upan);
		lt_MemoryPb = (ProgressBar) leftPart.findViewById(R.id.probar_memory);
		mGridViewLeft = (GridView) leftPart.findViewById(R.id.gridview_base);
		
		rt_UseMemoryUTv = (TextView) rightPart.findViewById(R.id.tv_memory_use_upan);
		rt_AvaMemoryUTv = (TextView) rightPart.findViewById(R.id.tv_memory_ava_upan);
		rt_UseMemoryTv = (TextView) rightPart.findViewById(R.id.tv_memory_use);
		rt_AvaMemoryTv = (TextView) rightPart.findViewById(R.id.tv_memory_ava);
		rt_MemoryUPb = (ProgressBar) rightPart.findViewById(R.id.probar_memory_upan);
		rt_MemoryPb = (ProgressBar) rightPart.findViewById(R.id.probar_memory);
		mGridViewRight = (GridView) rightPart.findViewById(R.id.gridview_base);
		//左面板按键
		lt_btBack = (Button) leftPart.findViewById(R.id.bt_base_back);
		lt_btOk = (Button) leftPart.findViewById(R.id.bt_base_ok);
		lt_btNext = (Button) leftPart.findViewById(R.id.bt_base_next);
		lt_btLast = (Button) leftPart.findViewById(R.id.bt_base_shang);
		//右面板按键
		rt_btBack = (Button) rightPart.findViewById(R.id.bt_base_back);
		rt_btOk = (Button) rightPart.findViewById(R.id.bt_base_ok);
		rt_btNext = (Button) rightPart.findViewById(R.id.bt_base_next);
		rt_btLast = (Button) rightPart.findViewById(R.id.bt_base_shang);
		baseView.addView(leftPart,params);
		baseView.addView(rightPart,params);
		setContentView(baseView);
	}
	
	/** 初始化界面的数据*/
	protected void initData() {
		//设置内部存储信息
		lt_AvaMemoryTv.setText("可用:" + FileUtils.getAvailableInternalMemorySize(this));
		lt_UseMemoryTv.setText("已用:" + FileUtils.getUseMemory(this));
		lt_MemoryPb.setMax(FileUtils.getTotalInternalMemorySize());
		lt_MemoryPb.setProgress(FileUtils.getUseMemory());
		mGridViewLeft.setSelector(R.drawable.list_item_selected);
		
		rt_AvaMemoryTv.setText("可用:" + FileUtils.getAvailableInternalMemorySize(this));
		rt_UseMemoryTv.setText("已用:" + FileUtils.getUseMemory(this));
		rt_MemoryPb.setMax(FileUtils.getTotalInternalMemorySize());
		rt_MemoryPb.setProgress(FileUtils.getUseMemory());
		
		//设置外部存储信息
		lt_AvaMemoryUTv.setText("可用:" + FileUtils.getAvailableExternalMemorySize(this));
		lt_UseMemoryUTv.setText("已用:" + FileUtils.getUseExternalMemorySize(this));
		lt_MemoryUPb.setMax(FileUtils.getTotalExternalMemorySize());
		lt_MemoryUPb.setProgress(FileUtils.getUseExternalMemorySize());
		
		rt_AvaMemoryUTv.setText("可用:" + FileUtils.getAvailableExternalMemorySize(this));
		rt_UseMemoryUTv.setText("已用:" + FileUtils.getUseExternalMemorySize(this));
		rt_MemoryUPb.setMax(FileUtils.getTotalExternalMemorySize());
		rt_MemoryUPb.setProgress(FileUtils.getUseExternalMemorySize());
		mGridViewRight.setSelector(R.drawable.list_item_selected);
	}
	
   /**
    * 设置事件监听
    */
	protected void setListener() {
		//左面板按键监听
		lt_btBack.setOnClickListener(this);
		lt_btNext.setOnClickListener(this);
		lt_btOk.setOnClickListener(this);
		lt_btLast.setOnClickListener(this);
		//右面板按键监听
		rt_btBack.setOnClickListener(this);
		rt_btNext.setOnClickListener(this);
		rt_btOk.setOnClickListener(this);
		rt_btLast.setOnClickListener(this);
	}

	/**
	 * 按钮点击事件处理
	 */
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.bt_base_back://返回键
			btBack();
			break;
        case R.id.bt_base_ok://确定键
			btOk();
			break;
		case R.id.bt_base_shang://上一步
			btLast();
			mGridViewLeft.smoothScrollToPosition(currentItem);
			mGridViewRight.smoothScrollToPosition(currentItem);
			break;
		case R.id.bt_base_next://下一步
			btNext();
			mGridViewLeft.smoothScrollToPosition(currentItem);
			mGridViewRight.smoothScrollToPosition(currentItem);
			break;
		}
	}

	/** 下一个按钮处理逻辑 */
	protected void btNext() {}

	/** 上一个按钮处理逻辑 */
	protected void btLast() {
		currentItem--;
		if(currentItem >= 0){
			mGridViewLeft.setSelection(currentItem);
			mGridViewRight.setSelection(currentItem);
		}
	}
    
	/** 确定按钮处理逻辑 */
	protected void btOk() {}

	/** 返回按钮处理逻辑 */
	protected void btBack() {}
	
}
