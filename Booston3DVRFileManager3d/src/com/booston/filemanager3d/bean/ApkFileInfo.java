package com.booston.filemanager3d.bean;

import java.io.File;

import android.graphics.drawable.Drawable;

/**
* @author 陈红华 E-mail:cenghonho@126.com
* @version 创建时间：2016年4月12日 上午10:03:58
* 类说明：apk信息封装类
*/
public class ApkFileInfo {
	
	/** apk图标 */
	public Drawable icon;
	
	/** apk路径 */
	public String path;
	
	/** apk文件名字*/
	public String name;
	
	/** apk名字*/
	public String apkName;
	
	/** apk安装包文件*/
	public File file;

}
