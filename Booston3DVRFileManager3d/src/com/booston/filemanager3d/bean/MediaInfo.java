package com.booston.filemanager3d.bean;

import android.graphics.Bitmap;

/**
 * 多媒体信息封装类
 */
public class MediaInfo {
	
	/** 文件标识 */
	public int id;   
	
	/** 文件的绝对路径 */
	public String path;
	
	/** 多媒体文件缩略图 */
	public Bitmap bitmap;
	
	/** 文件名*/
	public String title;

	public MediaInfo(int id, String path, Bitmap bitmap, String title) {
		this.id = id;
		this.path = path;
		this.bitmap = bitmap;
		this.title = title;
	}
	
	public MediaInfo(){}
	
}
