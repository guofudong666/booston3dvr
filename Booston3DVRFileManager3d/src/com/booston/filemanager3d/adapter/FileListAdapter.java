package com.booston.filemanager3d.adapter;

import java.util.List;
import java.util.Map;

import com.booston.filemanager3d.R;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
* @author 陈红华 E-mail:cenghonho@126.com
* @version 创建时间：2016年4月13日 下午1:57:48
* 类说明:全部界面中文件列表的数据适配器
*/
public class FileListAdapter extends BaseAdapter{  
	
    private LayoutInflater inflater;  
    private Bitmap directory,file;  
    //存储文件名称  
    private List<Map<String, Object>> datas;
	private int currentItem;  
    //参数初始化  
    public FileListAdapter(Context context,List<Map<String, Object>> datas){  
        this.datas = datas;
        directory = BitmapFactory.decodeResource(context.getResources(),R.drawable.dirfile);  
        file = BitmapFactory.decodeResource(context.getResources(),R.drawable.hdinput);  
        //缩小图片  
        inflater = LayoutInflater.from(context);  
    }  
    
    public void setSelectPosition(int position){
    	currentItem = position;
    	notifyDataSetChanged();
    }
    @Override  
    public int getCount() {  
        return datas.size();  
    }  
  
    @Override  
    public Object getItem(int position) {  
        return position;  
    }  
  
    @Override  
    public long getItemId(int position) {  
        return position;  
    }  
  
    @Override  
    public View getView(int position, View convertView, ViewGroup parent) {  
        ViewHolder holder;  
        if (null == convertView){  
            convertView = inflater.inflate(R.layout.gridview_item, null);  
            holder = new ViewHolder();  
            holder.text = (TextView)convertView.findViewById(R.id.title);  
            holder.image = (ImageView)convertView.findViewById(R.id.icon_view); 
            holder.mLinLayBg = (LinearLayout) convertView.findViewById(R.id.lin);
            convertView.setTag(holder);  
        }  
        else {  
            holder = (ViewHolder)convertView.getTag();  
        }  
        Map<String, Object> map = datas.get(position); 
        holder.image.setImageResource((Integer) map.get("icon"));
        holder.text.setText((String) map.get("fileName"));
        if(position == currentItem){
        	holder.mLinLayBg.setEnabled(true);
        }else{
        	holder.mLinLayBg.setEnabled(false);
        }
        return convertView;  
    }  
    private class ViewHolder{  
        private TextView text;  
        private ImageView image;  
        private LinearLayout mLinLayBg;
    }  
}  