package com.booston.filemanager3d.adapter;

import java.util.ArrayList;

import com.booston.filemanager3d.R;
import com.booston.filemanager3d.bean.MediaInfo;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * 多媒体（音乐列表，视频列表，图片列表）数据适配器
 */
public class MediaAdapter extends BaseAdapter {
	
	private Context context;
	private ArrayList<MediaInfo> videoList;
	private int currentItem;

	public MediaAdapter(Context ctx,ArrayList<MediaInfo> list) {
		this.context = ctx;
		this.videoList = list;
	}

	public void setSelectPosition(int position){
		currentItem = position;
		notifyDataSetChanged();
	}
	@Override
	public int getCount() {
		return videoList.size();
	}

	@Override
	public MediaInfo getItem(int position) {
		return videoList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		if (convertView == null) {
			convertView = View.inflate(context, R.layout.gridview_item, null);
			holder = new ViewHolder(convertView);
		}else {
			holder = (ViewHolder) convertView.getTag();
		}
		MediaInfo item = getItem(position);
		if(item.bitmap == null){
			//没有缩略图设置一张默认的图片
			holder.ivPic.setImageBitmap(BitmapFactory.decodeResource(
					context.getResources(), R.drawable.mediaplayer));
		}else{
			holder.ivPic.setImageBitmap(item.bitmap);
		}
		holder.tvTitle.setText(item.title);
		
		//设置当前选中item的背景
		if(currentItem == position){
			holder.mLinLayBg.setEnabled(true);
		}else{
			holder.mLinLayBg.setEnabled(false);
		}
		return holder.convertView;
	}
	
	
	static class ViewHolder{
		
		public ImageView ivPic;
		public TextView tvTitle;
		public LinearLayout mLinLayBg;
		public View convertView;
		
		public ViewHolder(View convertView){
			this.convertView = convertView;
			ivPic = (ImageView) convertView.findViewById(R.id.icon_view);
			tvTitle = (TextView) convertView.findViewById(R.id.title);
			mLinLayBg = (LinearLayout) convertView.findViewById(R.id.lin);
			convertView.setTag(this);
		}
	}

}
