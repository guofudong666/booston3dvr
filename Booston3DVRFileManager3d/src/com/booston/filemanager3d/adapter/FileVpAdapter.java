package com.booston.filemanager3d.adapter;

import java.util.ArrayList;
import java.util.List;

import com.booston.filemanager3d.R;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

/**
* @author 陈红华 E-mail:cenghonho@126.com
* @version 创建时间：2016年4月11日 下午4:06:31
* 类说明：文件管理界面ViewPager的适配器
*/
public class FileVpAdapter extends PagerAdapter {
	// 存放view的集合
	List<View> viewContains = new ArrayList<View>();
	public  LinearLayout src;
	public  LinearLayout src1;
	public  LinearLayout src2;
	public  LinearLayout src3;
	public  LinearLayout src4;
	private int curSelectedIndex = 0;
	/**
	 * 解析viewpage_layout，添加到集合中
	 */
	public FileVpAdapter(Context context) {
		View view=View.inflate(context, R.layout.viewpage_layout, null);
		View view2=View.inflate(context, R.layout.viewpage_layout2, null);
		this.viewContains.add(view);
		this.viewContains.add(view2);
		src = (LinearLayout)view.findViewById(R.id.src);
		src1 = (LinearLayout)view.findViewById(R.id.src1);
		src2= (LinearLayout)view.findViewById(R.id.src2);
		src3 = (LinearLayout)view2.findViewById(R.id.src3);
		src4= (LinearLayout)view2.findViewById(R.id.src4);	
		src.setBackgroundResource(R.drawable.list_item_selected);
		src1.setBackgroundResource(R.drawable.selected);
		src2.setBackgroundResource(R.drawable.selected);
		src3.setBackgroundResource(R.drawable.selected);
		src4.setBackgroundResource(R.drawable.selected);
	}
	/**
	 * 把集合中的View添加到父容器中，返回一个布局
	 */
	@Override
	public Object instantiateItem(ViewGroup container, int position) {
		switch (curSelectedIndex) {
		case 0:
			src.setBackgroundResource(R.drawable.list_item_selected);
			src1.setBackgroundResource(R.drawable.selected);
			src4.setBackgroundResource(R.drawable.selected);
			break;
		case 1:
			src.setBackgroundResource(R.drawable.selected);
			src1.setBackgroundResource(R.drawable.list_item_selected);
			src2.setBackgroundResource(R.drawable.selected);
			break;
		case 2:
			src1.setBackgroundResource(R.drawable.selected);
			src2.setBackgroundResource(R.drawable.list_item_selected);
			src3.setBackgroundResource(R.drawable.selected);
			break;
		case 3:
			src2.setBackgroundResource(R.drawable.selected);
			src3.setBackgroundResource(R.drawable.list_item_selected);
			src4.setBackgroundResource(R.drawable.selected);
			break;
		case 4:
			src.setBackgroundResource(R.drawable.selected);
			src3.setBackgroundResource(R.drawable.selected);
			src4.setBackgroundResource(R.drawable.list_item_selected);
			break;
		}
		container.addView(this.viewContains.get(position));
		return this.viewContains.get(position);
	}

	/**
	 * 当页面销毁则移除布局
	 */
	@Override
	public void destroyItem(ViewGroup container, int position, Object object) {
		container.removeView(this.viewContains.get(position));
	}

	/**
	 * 一共有多少页面
	 */
	@Override
	public int getCount() {
		return this.viewContains.size();
	}

	@Override
	public boolean isViewFromObject(View arg0, Object arg1) {
		return arg0 == arg1;
	}

	public void setCurSelectedIndex(int curSelectedIndex) {
		this.curSelectedIndex = curSelectedIndex;
	}
}
