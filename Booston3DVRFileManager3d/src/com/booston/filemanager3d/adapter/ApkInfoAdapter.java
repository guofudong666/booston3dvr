package com.booston.filemanager3d.adapter;

import java.util.List;

import com.booston.filemanager3d.R;
import com.booston.filemanager3d.bean.ApkFileInfo;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
* @author 陈红华 E-mail:cenghonho@126.com
* @version 创建时间：2016年4月12日 上午10:26:36
* 类说明
*/
public class ApkInfoAdapter extends BaseAdapter{
	
	private Context context;
	private List<ApkFileInfo> data;
	private int currentItem;
	
	public ApkInfoAdapter(Context context,List<ApkFileInfo> data) {
		this.context = context;
		this.data = data;
	}
	public void setSelectPosition(int position){
		currentItem = position;
		notifyDataSetChanged();
	}
	@Override
	public int getCount() {
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		return position;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;
		if(convertView == null){
			convertView = View.inflate(context, R.layout.gridview_item, null);
			holder = new ViewHolder(convertView);
		}else{
			holder = (ViewHolder) convertView.getTag();
		}
		holder.mImage.setImageDrawable(data.get(position).icon);
		holder.mText.setText(data.get(position).apkName);
		if(currentItem == position){
			holder.mLinLayBg.setEnabled(true);
		}else{
			holder.mLinLayBg.setEnabled(false);
		}
		return holder.convertView;
	}
	
	class ViewHolder{
		public View convertView;
		public ImageView mImage;
		public TextView mText;
		public LinearLayout mLinLayBg;
		public ViewHolder(View convertView){
			this.convertView = convertView;
			convertView.setTag(this);
			mLinLayBg = (LinearLayout) convertView.findViewById(R.id.lin);
			mImage = (ImageView) convertView.findViewById(R.id.icon_view);
			mText = (TextView) convertView.findViewById(R.id.title);
		}
	}

}
