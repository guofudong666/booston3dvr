package com.booston.filemanager3d.utils;

import java.io.File;
import java.util.ArrayList;

import com.booston.filemanager3d.bean.MediaInfo;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaMetadataRetriever;
import android.provider.MediaStore;
import android.provider.MediaStore.Images;

/**
* @author 陈红华 E-mail:cenghonho@126.com
* @version 创建时间：2016年4月14日 下午1:14:42
* 类说明:多媒体工具类
*/
public class MediaUtils {
	
	/**
	 * 通过文件名 获取音乐的缩略图
	 * @param context
	 * @param cr : getContentResolver();
	 * @param testVideopath
	 *            全路径 "/mnt/sdcard/sidamingbu.mp3";
	 * @return
	 */
	public static ArrayList<MediaInfo> getAudioList(Context context) {
		ArrayList<MediaInfo> audioList = new ArrayList<MediaInfo>();
		ContentResolver testcr = context.getContentResolver();
		String[] projection = { MediaStore.Audio.Media.DATA,
				MediaStore.Audio.Media._ID, MediaStore.Audio.Media.TITLE };
		Cursor cursor = testcr.query(
				MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, projection, null,
				null, null);
		System.out.println(cursor.getCount());
		if (cursor == null || cursor.getCount() == 0) {
			return null;
		}
		while (cursor.moveToNext()) {
			String path = cursor.getString(0);
			int id = cursor.getInt(1);
			String title = cursor.getString(2);
			// 能够获取多媒体文件元数据的类
			MediaMetadataRetriever retriever = new MediaMetadataRetriever();
			Bitmap bitmap = null;
			try {
				retriever.setDataSource(path); // 设置数据源
				byte[] art = retriever.getEmbeddedPicture(); // 得到字节型数据
				bitmap = BitmapFactory.decodeByteArray(art, 0, art.length); // 转换为图片
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				try {
					retriever.release();
				} catch (Exception e2) {
					e2.printStackTrace();
				}
			}
			MediaInfo audioInfo = new MediaInfo(id, path, bitmap, title);
            audioList.add(audioInfo);
		}
		return audioList;
	}
	
	/** 
     * 通过文件名 获取视频的缩略图 
     * @param context 
     * @param cr     cr = getContentResolver(); 
     * @param testVideopath  全路径 "/mnt/sdcard/sidamingbu.mp4"; 
     * @return 
     */  
    public static ArrayList<MediaInfo> getVideoList(Context context) {  
    	ArrayList<MediaInfo> videoList = new ArrayList<MediaInfo>();
        ContentResolver testcr = context.getContentResolver();  
        String[] projection = { MediaStore.Video.Media.DATA, MediaStore.Video.Media._ID, MediaStore.Video.Media.TITLE};  
        Cursor cursor = testcr.query(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, projection, null,  
                null, null);  
        System.out.println(cursor.getCount());
        if (cursor == null || cursor.getCount() == 0) {  
            return null;  
        }  
        
		while (cursor.moveToNext()) {
			String path = cursor.getString(0);
			int id = cursor.getInt(1);
			String title = cursor.getString(2);
			BitmapFactory.Options options = new BitmapFactory.Options();
			options.inDither = false;
			options.inPreferredConfig = Bitmap.Config.ARGB_8888;
			Bitmap bitmap = MediaStore.Video.Thumbnails.getThumbnail(testcr,
					id, Images.Thumbnails.MICRO_KIND, options);
			MediaInfo videoInfo = new MediaInfo(id, path, bitmap, title);
			videoList.add(videoInfo);
		}
       return videoList;
    }  
    
}
