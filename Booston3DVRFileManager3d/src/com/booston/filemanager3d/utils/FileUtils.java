package com.booston.filemanager3d.utils;

import java.io.File;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ThumbnailUtils;
import android.os.Environment;
import android.os.StatFs;
import android.text.format.Formatter;

/**
* @author 陈红华 E-mail:cenghonho@126.com
* @version 创建时间：2016年4月13日 下午2:21:53
* 类说明
*/
public class FileUtils {
	
	/**
	 * 判断文件是否为apk<br>
	 */
	public static boolean isApk(String pInput ) {
		// 文件名称为空的场合
		if (pInput.equals(null)) {
			// 返回不和合法
			return false;
		}
		// 获得文件后缀名
		String tmpName = pInput.substring(pInput.lastIndexOf(".") + 1, pInput.length());
		// 声明图片后缀名数组
		String imgeArray[] = { "apk" };
		// 遍历名称数组
		for (int i = 0; i < imgeArray.length; i++) {
			// 判断
			if (imgeArray[i].equals(tmpName.toLowerCase())) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * 判断文件是否为图片<br>
	 */
	public static boolean isPicture(String pInput ) {
		// 文件名称为空的场合
		if (pInput.equals(null)) {
			// 返回不和合法
			return false;
		}
		// 获得文件后缀名
		String tmpName = pInput.substring(pInput.lastIndexOf(".") + 1, pInput.length());
		// 声明图片后缀名数组
		String imgeArray[] = { "bmp", "dib", "gif", "jfif", "jpe", "jpeg", "jpg", "png", "tif", "tiff", "ico" };
		// 遍历名称数组
		for (int i = 0; i < imgeArray.length; i++) {
			// 判断
			if (imgeArray[i].equals(tmpName.toLowerCase())) {
				return true;
			}
		}
		return false;
	}
	
	 /** 
     * 根据指定的图像路径和大小来获取缩略图 
     * 此方法有两点好处： 
     *     1. 使用较小的内存空间，第一次获取的bitmap实际上为null，只是为了读取宽度和高度， 
     *        第二次读取的bitmap是根据比例压缩过的图像，第三次读取的bitmap是所要的缩略图。 
     *     2. 缩略图对于原图像来讲没有拉伸，这里使用了2.2版本的新工具ThumbnailUtils，使 
     *        用这个工具生成的图像不会被拉伸。 
     * @param imagePath 图像的路径 
     * @param width 指定输出图像的宽度 
     * @param height 指定输出图像的高度 
     * @return 生成的缩略图 
     */  
    public static Bitmap getImageThumbnail(String imagePath, int width, int height) {  
        Bitmap bitmap = null;  
        BitmapFactory.Options options = new BitmapFactory.Options();  
        options.inJustDecodeBounds = true;  
        // 获取这个图片的宽和高，注意此处的bitmap为null  
        bitmap = BitmapFactory.decodeFile(imagePath, options);  
        options.inJustDecodeBounds = false; // 设为 false  
        // 计算缩放比  
        int h = options.outHeight;  
        int w = options.outWidth;  
        int beWidth = w / width;  
        int beHeight = h / height;  
        int be = 1;  
        if (beWidth < beHeight) {  
            be = beWidth;  
        } else {  
            be = beHeight;  
        }  
        if (be <= 0) {  
            be = 1;  
        }  
        options.inSampleSize = be;  
        // 重新读入图片，读取缩放后的bitmap，注意这次要把options.inJustDecodeBounds 设为 false  
        bitmap = BitmapFactory.decodeFile(imagePath, options);  
        // 利用ThumbnailUtils来创建缩略图，这里要指定要缩放哪个Bitmap对象  
        bitmap = ThumbnailUtils.extractThumbnail(bitmap, width, height,  
                ThumbnailUtils.OPTIONS_RECYCLE_INPUT);  
        return bitmap;  
    }  
    
    /**
     * 获取手机内部剩余存储空间
     * @return
     */
    public static String getAvailableInternalMemorySize(Context context) {
        File path = Environment.getDataDirectory();
        StatFs stat = new StatFs(path.getPath());
        long blockSize = stat.getBlockSizeLong();
        long availableBlocks = stat.getAvailableBlocksLong();
        return Formatter.formatFileSize(context,availableBlocks * blockSize);
    }

    /**
     * 获取手机内部总的存储空间
     * @return
     */
    public static String getTotalInternalMemorySize(Context context) {
        File path = Environment.getDataDirectory();
        StatFs stat = new StatFs(path.getPath());
        long blockSize = stat.getBlockSizeLong();
        long totalBlocks = stat.getBlockCountLong();
        return Formatter.formatFileSize(context, blockSize * totalBlocks);
    }
    

    /**
     * 获取手机内部总的存储空间
     * @return
     */
    public static int getTotalInternalMemorySize() {
        File path = Environment.getDataDirectory();
        StatFs stat = new StatFs(path.getPath());
        long blockSize = stat.getBlockSizeLong();
        long totalBlocks = stat.getBlockCountLong();
        return (int) (blockSize * totalBlocks / 1024f + 0.5f);
    }
    
    /**
     * 获取手机内部已用的内存
     * @param context
     * @return
     */
    public static String getUseMemory(Context context){
    	 File path = Environment.getDataDirectory();
         StatFs stat = new StatFs(path.getPath());
         long blockSize = stat.getBlockSizeLong();
         long totalBlocks = stat.getBlockCountLong();
         long availableBlocks = stat.getAvailableBlocksLong();
    	return  Formatter.formatFileSize(context, blockSize * (totalBlocks - availableBlocks));
    }
    
    /**
     * 获取手机内部已用的内存
     * @param context
     * @return
     */
    public static int getUseMemory(){
    	 File path = Environment.getDataDirectory();
         StatFs stat = new StatFs(path.getPath());
         long blockSize = stat.getBlockSizeLong();
         long totalBlocks = stat.getBlockCountLong();
         long availableBlocks = stat.getAvailableBlocksLong();
    	return  (int) (blockSize * (totalBlocks - availableBlocks) / 1024f + 0.5f);
    }
    
    
    /**
     * 检测Sd卡是否可用
     * @return
     */
    public static boolean externalMemoryAvailable(){  
        return Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED);  
    }  
     
    /**
     * 获取外部可用内存的大小
     * @param context
     * @return
     */
	public static String getAvailableExternalMemorySize(Context context){  
	        if(externalMemoryAvailable()){  
	            File path = Environment.getExternalStorageDirectory();  
	            StatFs stat = new StatFs(path.getPath());  
	            long blockSize = stat.getBlockSizeLong();  
	            long availableBlocks = stat.getAvailableBlocksLong();  
	            return Formatter.formatFileSize(context, availableBlocks*blockSize);
	        }  
	        else{  
	            return null;  
	        }  
	}  
	      
	/**
	 * 获取外部存储已用的内存大小
	 * @return
	 */
	public static int getUseExternalMemorySize(){  
	        if(externalMemoryAvailable()){  
	            File path = Environment.getExternalStorageDirectory();  
	            StatFs stat = new StatFs(path.getPath());  
	            long blockSize = stat.getBlockSizeLong();  
	            long totalBlocks = stat.getBlockCountLong(); 
	            long availableBlocks = stat.getAvailableBlocksLong(); 
	            return (int) ((totalBlocks - availableBlocks)*blockSize / 1024f + 0.5f);  
	        }  
	        else{  
	            return -1;  
	        }  
	}  
	
	/**
	 * 获取外部可用内存的大小
	 * @return
	 */
	public static long getAvailableExternalMemorySize(){  
        if(externalMemoryAvailable()){  
            File path = Environment.getExternalStorageDirectory();  
            StatFs stat = new StatFs(path.getPath());  
            long blockSize = stat.getBlockSizeLong();  
            long availableBlocks = stat.getAvailableBlocksLong();  
            return (long) (availableBlocks*blockSize / 1024f + 0.5f);  
        }  
        else{  
            return -1;  
        }  
    }  
      
	/**
	 * 获取外部存储已用内存的大小
	 * @param context
	 * @return
	 */
	public static String getUseExternalMemorySize(Context context){  
	        if(externalMemoryAvailable()){  
	            File path = Environment.getExternalStorageDirectory();  
	            StatFs stat = new StatFs(path.getPath());  
	            long blockSize = stat.getBlockSizeLong();  
	            long totalBlocks = stat.getBlockCountLong();
	            long availableBlocks = stat.getAvailableBlocksLong(); 
	            return Formatter.formatFileSize(context, (totalBlocks - availableBlocks)*blockSize);  
	        }  
	        else{  
	            return null;  
	        }  
   }  
	
	/**
	 * 获取外部存储总的容量
	 * @return
	 */
	public static int getTotalExternalMemorySize(){  
        if(externalMemoryAvailable()){  
            File path = Environment.getExternalStorageDirectory();  
            StatFs stat = new StatFs(path.getPath());  
            long blockSize = stat.getBlockSizeLong();  
            long totalBlocks = stat.getBlockCountLong();  
            return (int) (totalBlocks*blockSize / 1024f + 0.5f);  
        }  
        else{  
            return -1;  
        }  
   }  
    
}
